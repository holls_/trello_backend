const mongoose = require('mongoose')
mongoose.connect(`mongodb+srv://hols1:${process.env.mongo_pass_hols1}@trelloclonebackend-kbb1s.mongodb.net/Trello_Clone?retryWrites=true&w=majority`, { useNewUrlParser: true })

const con = mongoose.connection
con.on('error', (err) => {
    console.log(err, "Erro no Mongo DB")
})
con.once('open', () => {
    console.log(`Mongo server running on ${con.db.databaseName} db`)
    con.db.collections().then((collections) => {
        console.log("Collections: ")
        collections.forEach(element => {
            console.log(element.namespace)
        });
    })
})

module.exports = {
    conn: con,
    Schema: mongoose.Schema,
    Model: mongoose.model
}