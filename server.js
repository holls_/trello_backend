const express = require('express')
const app = express()

/** NPM's */
const cors = require("cors")
const bodyParser = require("body-parser")
/** Controllers */
const boardController = require('./src/controllers/board.controller')
const listController = require("./src/controllers/list.controller")

//Middleware
app.use(cors())
app.use(bodyParser.json()) // create application/json parser
app.use(bodyParser.urlencoded({extended: false}))// create application/x-www-form-urlencoded parser

/** Requests */
/** Não sei se vou implementar passwords ou tokens aqui... */
app.get("/", (req, res) => {
    res.send(`
    <h1>"Página" inicial do trello back end clone</h1>
    <ul>
        <li>Acabar a conexão ao Mongo Atlas</li>
    </ul>
    `)
})

/** CRUD para os Boards e para as notas */
/** Boards */
app.get("/boards/getall", (req, res) => {
    boardController.getAll(res);
})
app.get("/boards/getone/:id", (req, res) => {
    boardController.getOne(res, req.params.id)
})

app.post("/boards/create", (req, res) => {
    boardController.create(res, req.body)
})
app.put("/boards/update/:id", (req, res) => {

})
app.delete("/boards/delete/:id", (req, res) => {

})

/** Lists */
app.get("/lists/getall", (req, res) => {
    listController.getAll(res)
})
app.get("/lists/getone/:id", (req, res) => {

})
app.get("/lists/getbyboard/:id", (req, res) => {
    listController.getByBoard(res, req.params.id)
})
app.post("/lists/create", (req, res) => {
    listController.create(res, req.body)    
})
app.put("/lists/update/:id", (req, res) => {

})
app.delete("/lists/delete/:id", (req, res) => {

})
app.post("/lists/addnote/:id", (req, res) => {
    listController.addNote(res, req.params.id, req.body.note)
})
module.exports = app