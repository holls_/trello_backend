const http = require('http')
require("dotenv").config()

const app = require('./server')
const {conn} = require("./mongo_connect")

let port = 8181;
let server = http.createServer(app)
server.listen(port, "192.168.1.83",() => console.log(`Servidor a correr na porta ${port} e em localhost`))