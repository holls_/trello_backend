const Board = require("../models/board.model")

const crudBoard = {
    getAll(res) {
        try {
            Board.find({}, (err, collection) => {
                if (err) throw err
                if (collection.length == 0) res.json({ msg: "Não há resultadoses", success: true }) // Mais uma vez dava para criar um fcheiro só com handleres para estas mensagens
                else {
                    console.log("getAll()")
                    res.json({ msg: `Encontrados ${collection.length}`, success: true, result: collection }) //Arranjar um nome melhor para o result.....
                }
            })

        }
        catch (err) {
            res.json({ msg: "Ocorreu un error no getAll() - Boards", success: false, error: err })
        }
    },
    getAllByUser(res) {

    },
    getOne(res, id) {
        if(id == null) return res.json({msg: "O id está a null", success: false})
        try {
            Board.findById(id, (err, result) => {
                if(err) throw err
                res.json({msg: "Board encontrada com successo", success: true, result: result})
            })
        } catch (err) {
            res.json({msg: "Ocorreu um erro no getOne() - Boards", success: false, error: err})
        }
    },
    create(res, content) {
        /** fazer a confirmação se o nome já existe (convertido para maiusculas) 
         * Depois esta confirmação vai ser feita por nome e por user
         * porque pode haver 20 boards com  nome igual desde que seja de 20 users diferentes
        */
        console.log(content, "content!!!!")
        /** Podia ter feito isto com um if */
        if (content.name == "") return res.json({msg: "Empty Name!", success: false, result: content}) //Isto não se devia chamar result
        try {
            Board.find({ name: content.name }, (err, collection) => {
                if (err) throw err
                if (collection.length == 0) {
                    let newBoard = Board({
                        name: content.name
                    })
                    newBoard.save(err => {
                        if (err) throw err
                        res.json({ msg: "Board criada com sucesso", success: true, result: newBoard })
                    })
                }
            })
        } catch (err) {
            res.json({ msg: "Ocorreu um erro no create() - Boards", success: false, error: err })
        }
    },
    update(res) {

    },
    delete(res) {

    }
}

module.exports = crudBoard;