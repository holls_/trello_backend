const List = require("../models/list.model")

const crudList = {
    getAll(res) {
        try {
            List.find({}, (err, collection) => {
                if (err) throw err
                res.json({ msg: `Foram encontrados ${collection.length}`, success: true, result: collection })
            })
        } catch (err) {
            res.json({ msg: "Ocorreu um erro em getAll() - list", success: false, error: err })
        }
    },
    getOne(res) {

    },
    getByBoard(res, boardId) {
        try {
            List.find({ boardId: boardId }, (err, collection) => {
                if (err) throw err;
                res.json({ msg: `Foram encontrados ${collection.length} resultados`, success: true, result: collection })
            })
        } catch (err) {
            res.json({ msg: "Ocorreu um erro no getByBoard() - lists", success: false, error: err })
        }
    },
    create(res, content) {
        console.log(content, "Content in create() - list")
        if (content == {}) return res.json({ msg: "Envias te um objeto vazio", success: false })
        try {
            List.find({ name: content.name }, (err, collection) => {
                if (err) throw err
                if (collection.length == 0) {
                    let newList = new List({
                        name: content.name,
                        boardId: content.boardId
                    })
                    newList.save(err => {
                        if (err) throw err
                        else res.json({ msg: "Lista criada com sucesso", success: true, result: newList })
                    })
                }
                else {
                    res.json({ msg: "Já existe uma lista com esse nome", success: false, result: collection[0] })
                }
            })
        } catch (err) {
            res.json({ msg: "Ocorreu um erro no create() - List", success: false, error: err })
        }
    },
    update(res) {
        /** Update para o nome (só) */
    },
    delete(res) {

    },
    addNote(res, _listId, note) {
        console.log("id da lista no controller", _listId);
        console.log("note !!! ", note)

        try {
            (async () => {
                let notasarr = []
                await List.find({}, (err, collection) => {
                    if(err) throw err 
                    else {
                        for(let list of collection) {
                            for(nota of list.notas) {
                                notasarr.push(nota)
                            }
                        }
                    }
                })
                await List.findById(_listId, (err, list) => {
                    if (err) throw err
                    console.log(list, "Isto é a lista onde se vai meter a nota")

                    if (note) {
                        /** Como fazer a confirmação que todos os campos obrigatórios 
                         * estão presentes????
                         * Confimar aqui ou no mongo???
                         * Devia ser feito no mongo, mas como ao certo.....
                         */
                        /** Adicionar data */
                        note.date = new Date().toISOString()

                        /** Como as notas vão estar dentro de uma certa lista, 
                         * ams podem ir de uma lista para outra, 
                         * então tem que se verificar que o id que se atribui a uma nova nota
                         * ainda não foi atribuida em mais lado nenhum,
                         * o mais fácil para se fazer isto era criar um objectId,
                         * no mongo que traatva disso, maaaaaas.......
                         * 
                         * Maneira 1: 
                         * - fazer uma query por todos os notas de um board e adicionar um id depois
                         * de saber quais são os ids que já existam. 
                        */
                        // Meter a nota no array de notas
                        console.log(notasarr, "array com todas as notas de um board")
                        note.id = notasarr.length == 0 ? 1 : notasarr[notasarr.length - 1].id + 1
                        console.log(note, "nota antes de ir para o array de notas da lista!!!!")
                        list.notas.push(note)
                        list.save(err => {
                            if (err) throw err;
                            res.json({ msg: "Nota inserida com sucesso", success: true, result: list })
                        })
                    }
                })
            })().catch(err => console.log(err))
        }
        catch (err) {
            res.json({ msg: "Ocorreu um erro no addNote() - no list.controller", success: false, error: err })
        }
    },
    removeNote(res, _listId, note) {

    }
}

module.exports = crudList;