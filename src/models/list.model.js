const { Model, Schema } = require("../../mongo_connect")

/** Ver as notas do board.model.js */
const listSchema = Schema({
    /** id atribuido automaticamente */
    name: {
        type: String,
        required: true
    },
    settings: {
        type: Object,
        default: {} // Não sei se vai funcinar assim, mas é suposto 
    },
    notas: {
        type: Array
        /** Notas structure:
         * id,
         * tipo (devia mandar isto a null até saber o que lhe fazer)
         * texto / conteudo
         * settings
         */
    },
    date: {
        type: Date,
        default: new Date().toISOString()
    },
    boardId: {
        type: String,
        // default: "5d2120c9509b9a175404dacf" // Está assim por agora só para testar, ou seja, todas as notas vão pertencer ao board 1
    }
})

const List = Model("Lists", listSchema);
module.exports = List; 