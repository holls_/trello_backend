const { Model, Schema } = require("../../mongo_connect");

/** Aqui vou criar um new Schema e Model também, 
 * mas nas Lists vou só usar Schema e Model (sem o new) 
 * para ver se à diferenças na base de dados, 
 * Sendo que nesta já estão criadas duas "tabelas"
 * com os nomes Boards e Lists,
 * Update: 
 *  Com o ((new) Schema) dá erro porque diz que o Schema não tem constructor
 */
const boardSchema = Schema({ 
    /** O id é atribuido automaticamente pelo mongoose (é o mongoose certo) */
    name: {
        type: String,
        required: true
    },
    date: {
        type: Date,
        default: new Date().toISOString() // Está atrasado, ver o video do CodingTrain onde ele muda o timezone na api (process.env???)
    }
    /** Devem faltar campos aqui */
})

const Board = new Model("Boards", boardSchema)
module.exports = Board;